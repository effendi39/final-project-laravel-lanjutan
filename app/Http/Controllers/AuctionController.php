<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuctionRequest;
use Illuminate\Http\Request;
use App\Models\Auction;

class AuctionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auction.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuctionRequest $request, $id)
    {
        $data = [];
        $auction = new Auction;
        $auction->item_id = $id;
        $auction->first_price = request('first_price');
        $auction->status = request('status');
        $auction->save();
        $data['item'] = $auction;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data auction berhasil dibuat',
            'data' => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuctionRequest $request, $id)
    {
        $data = [];
        $auction = Auction::FindOrFail($id);
        $auction->first_price = request('first_price');
        $auction->status = request('status');
        $auction->save();
        $data['item'] = $auction;

        return response()->json([
            'response_code' => '01',
            'response_message' => ' data auction berhasil di update',
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auction = Auction::FindOrFail($id);
        $auction->delete();
        return response()->json([
            'response_code' => '02',
            'response_message' => ' data auction berhasil di hapus',
        ], 200);
    }
}
