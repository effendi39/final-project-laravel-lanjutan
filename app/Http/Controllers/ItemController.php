<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use Illuminate\Http\Request;
use App\Models\Item;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemRequest $request)
    {
        $data = [];
        $item = new Item;
        $item->id = request('id');
        $item->name = request('name');
        $item->save();
        $data['item'] = $item;

        return response()->json([
            'response_code' => '00',
            'response_message' => ' data item berhasil dibuat',
            'data' => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ItemRequest $request, $id)
    {
        $data = [];
        $item = Item::FindOrFail($id);
        $item->name = request('name');
        $item->save();
        $data['item'] = $item;

        return response()->json([
            'response_code' => '01',
            'response_message' => ' data item berhasil di update',
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::FindOrFail($id);
        $item->delete();
        return response()->json([
            'response_code' => '03',
            'response_message' => ' data item berhasil di hapus'
        ], 200);
    }
}
