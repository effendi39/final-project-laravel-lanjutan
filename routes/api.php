<?php

Route::group(['middleware' => ['auth:api']], function () {
    // item
    Route::post('item', 'ItemController@store')->name('store');
    Route::put('update_item/{id}', 'ItemController@update')->name('update');
    Route::delete('delete_item/{id}', 'ItemController@destroy')->name('delete');

    // auction
    Route::post('auctions/{id}', 'AuctionController@store');
    Route::put('auctions_update/{id}', 'AuctionController@update');
    Route::delete('auctions_delete/{id}', 'AuctionController@destroy');
});
